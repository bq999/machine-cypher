//
//  missionViewController.swift
//  project2
//
//  Created by Bilal Qaiser on 23/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import CoreLocation
import MessageUI

class missionViewController: UITableViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate, MFMessageComposeViewControllerDelegate  {
    
    @IBOutlet weak var mapObj: MKMapView!
    
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var dataArray = [NSManagedObject]()
    
    var c = [String]()
    var c1 = UserDefaults.standard.value(forKey: kCountryStr) as? [String] ?? [String]()

    

    //cell.name.text = dataArray[indexPath.row].value(forKey: kCountryStr) as? String

    

    
    
    
    var address : [Address1] = []
    var annotations : [MKPointAnnotation] = []
    var mapManager = CLLocationManager()
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // let location = locations[0]
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(150, 150)
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(43.773257, -79.335899)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        mapObj.setRegion(region, animated: true)
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        
        
        self.dismiss(animated: true, completion: nil)

    }
    
    
    
    @IBAction func message(_ sender: Any) {
        
        if MFMessageComposeViewController.canSendText(){
            
            let controller = MFMessageComposeViewController()
            controller.body = "Hello"
            controller.recipients = ["647-666-9999"]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else {
            print("Error")
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Map Mission"
        
        
        
        mapManager.delegate = self                            // ViewController is the "owner" of the map.
        mapManager.desiredAccuracy = kCLLocationAccuracyBest  // Define the best location possible to be used in app.
        mapManager.requestWhenInUseAuthorization()            // The feature will not run in background
        mapManager.startUpdatingLocation()                    // Continuously geo-position update
        self.mapObj.showsUserLocation = true
        
        
        findAddress()
        
        
        

    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchData()
        
    }
    
    
    
    
    
    func fetchData() {
        let entityDescription = NSEntityDescription.entity(forEntityName: kEntityStr, in: appDelegateObj.managedObjectContext)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entityDescription
        

        do {
            dataArray = try appDelegateObj.managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]
            
            
            //print(c1)
            
            self.tableView.reloadData()
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! ProfileCell4
        
        cell.name.text = dataArray[indexPath.row].value(forKey: kCountryStr) as? String
        
        
        let cell2 = cell.name.text!
        
        //c1.append(cell2)
        
        /*
        for id in c {
            
            if c.contains("USA") {
                print(true)
         
            }
            
        }
        */

        //print(c1)
        return cell
        

        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            appDelegateObj.managedObjectContext.delete(dataArray[indexPath.row])
            do {
                try appDelegateObj.managedObjectContext.save()
                dataArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } catch {
                let saveError = error as NSError
                print(saveError)
            }
        }
    }
    
    
    
    
    
    func findAddress() {
        
        
        /*
        let entityDescription = NSEntityDescription.entity(forEntityName: kEntityStr, in: appDelegateObj.managedObjectContext)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entityDescription
        
        do {
            dataArray = try appDelegateObj.managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]

            let sortDescriptor = NSSortDescriptor(key:kCountryStr, ascending: true)
            print(sortDescriptor)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
*/


        let l2 = ["title": "New York, NY",    "latitude": 40.713054, "longitude": -74.007228, "color": "red"] as [String : Any]
        

       // print(c1)

        for id in c {
            
            if c.contains("USA") {
              //  print(l2)
            }
            
        }
        
        
       // var locations = [String]()
       
        
        var locations = [
            ["title": "New York, NY",    "latitude": 40.713054, "longitude": -74.007228, "color": "red"],
            ["title": "Los Angeles, CA", "latitude": 34.052238, "longitude": -118.243344, "color": "blue"],
            ["title": "Chicago, IL",     "latitude": 41.883229, "longitude": -87.632398, "color": "green"]
        ]
        
        locations.append(["title": "dfdf dfdf, dfdf",    "latitude": 40.713054, "longitude": -74.007228, "color": "red"])
        
       // locations.append("")

       // print(locations)
        
        for location in locations {
            
            let annotation = MKPointAnnotation()
            annotation.title = location["title"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: location["latitude"] as! Double, longitude: location["longitude"] as! Double)
            
            mapObj.addAnnotation(annotation)
            print(annotation)
        }
        
            }
    
    
    
    
    
    // dismiss the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
}

class ProfileCell4 : UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
}
