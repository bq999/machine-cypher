//
//  Address.swift
//  project2
//
//  Created by Bilal Qaiser on 20/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//


import Foundation

class Address {
    public private(set) var addressname    : String
    public private(set) var latitude       : Double
    public private(set) var longitude      : Double
    
    
    init(name: String, latitude: Double, longitude: Double){
        self.addressname = name
        self.latitude = latitude
        self.longitude = longitude
        
    }
}
