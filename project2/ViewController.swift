//
//  ViewController.swift
//  project2
//
//  Created by Bilal Qaiser on 17/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UITableViewController {
    
    
    
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var dataArray = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Agent Profile"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchData()
    }
    
    func fetchData() {
        let entityDescription = NSEntityDescription.entity(forEntityName: kEntityStr, in: appDelegateObj.managedObjectContext)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entityDescription
        
        do {
            dataArray = try appDelegateObj.managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]
            self.tableView.reloadData()
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfileCell

        cell.lblName.text = dataArray[indexPath.row].value(forKey: kNameStr) as? String

        
        
        return cell

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "SegueView", sender: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            appDelegateObj.managedObjectContext.delete(dataArray[indexPath.row])
            do {
                try appDelegateObj.managedObjectContext.save()
                dataArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } catch {
                let saveError = error as NSError
                print(saveError)
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueView" {
            let vc : ViewProfileVC = segue.destination as! ViewProfileVC
            vc.dataArray = dataArray
            vc.editRecNo = sender as! Int
            
        }
        
        if segue.identifier == "map" {
            let vc : map = segue.destination as! map
            vc.dataArray = dataArray
        }
        

        
    }
    
    @IBAction func btnAddTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "SegueAdd", sender: -1)
    }
    
    
    @IBAction func map(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "map", sender: -1)
    }
    
    

    @IBAction func map2(_ sender: Any) {
        self.performSegue(withIdentifier: "map2", sender: -1)

    }

    
    
    
}

class ProfileCell : UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!    
}
