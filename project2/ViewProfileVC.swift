//
//  ViewProfileVC.swift
//  project2
//
//  Created by Bilal Qaiser on 18/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//


import UIKit
import CoreData


class ViewProfileVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtMission: UILabel!
    @IBOutlet weak var txtCountry: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    
    
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var editRecNo = Int()
    var dataArray = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if editRecNo != -1 {
            txtName.text = self.dataArray[editRecNo].value(forKey: kNameStr) as? String
            txtMission.text = self.dataArray[editRecNo].value(forKey: kMissionStr) as? String
            txtCountry.text = self.dataArray[editRecNo].value(forKey: kCountryStr) as? String
            txtDate.text = self.dataArray[editRecNo].value(forKey: kDateStr) as? String
            
        }
        

        
/*
         
        txtName.text = self.dataArray[editRecNo].value(forKey: kNameStr) as? String

        let originalText = txtName.text!
        
        
        let cipheredText = cipher( originalText, shift:5 )
        print( "Ciphered text: \(cipheredText)" )
        
        let decipheredText = decipher( cipheredText, shift2:5 )
        print( "Decipher text: \(decipheredText)" )
 
 */
        /*
        txtName.text = self.dataArray[editRecNo].value(forKey: kNameStr) as? String
        */
        
        
        /*
        let originalText = txtName.text!
        
        print( "Ciphered text: \(originalText)" )
        
        let decipheredText = decipher( originalText, shift2:10 )
        print( "Decipher text: \(decipheredText)" )
 */
    }
    
    
    
    
    
    func cipher( _ text:String, shift:Int ) -> String {
        
        var textCharArray:[Character] = Array( text.characters )
        
        let alphabet =
            Array("0123456789abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUWVXYZ !".characters);
        
        let offset = alphabet.count // alphabet.length in other languages
        
        for i in 0 ..< text.characters.count {
            
            let oldChar = textCharArray[ i ]
            let oldCharIdx = alphabet.index( of:oldChar ) // get index of
            
            let oldCharIdxUnwrapped = oldCharIdx! // oldCharIdx can be null!
            let newCharIdx = ( oldCharIdxUnwrapped + shift + offset ) % offset
            
            let newChar = alphabet[ newCharIdx ]
            
            textCharArray[ i ] = newChar
        }
        
        return String( textCharArray )
    }
    
    func decipher( _ text:String, shift2:Int ) -> String {
        
        // when calling a function you don't have to specify the first argument...
        // For other arguments you have to specify it!
        return cipher( text, shift:shift2 * -1 )
    }
    

    
    @IBAction func btnCancelTap(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

