//
//  AddViewController.swift
//  project2
//
//  Created by Bilal Qaiser on 17/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//

import UIKit
import CoreData



class AddViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtMission: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtDate: UITextField!

    @IBOutlet weak var dropdown1: UIPickerView!
    @IBOutlet weak var dropdown2: UIPickerView!
 
    
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var editRecNo = Int()
    var dataArray = [NSManagedObject]()
    
    var mission = ["I","R","P"]
    var country = ["USA","Canada","Pakistan","India","Mexico","Qatar","Carribean","Africa","Rome"]
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        createDatePicker()
        
        // Input data into the Array:

    }
        /*
        if editRecNo != -1 {
            txtName.text = self.dataArray[editRecNo].value(forKey: kNameStr) as? String
            txtMission.text = self.dataArray[editRecNo].value(forKey: kMissionStr) as? String
            txtCountry.text = self.dataArray[editRecNo].value(forKey: kCountryStr) as? String
            txtDate.text = self.dataArray[editRecNo].value(forKey: kDateStr) as? String

        }
 */
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return (true)
    }
    

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var countrows : Int = mission.count
        if pickerView == dropdown2 {
            
            countrows = self.country.count
        }
        
        return countrows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == dropdown1 {
            
            let titleRow = mission[row]
            
            return titleRow
            
        }
            
        else if pickerView == dropdown2{
            let titleRow = country[row]
            
            return titleRow
        }
        
        return ""
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == dropdown1 {
            self.txtMission.text = self.mission[row]
            self.dropdown1.isHidden = true
        }
            
        else if pickerView == dropdown2{
            self.txtCountry.text = self.country[row]
            self.dropdown2.isHidden = true
            
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == self.txtMission){
            self.dropdown1.isHidden = false
            
        }
        else if (textField == self.txtCountry){
            self.dropdown2.isHidden = false
            
        }
        
    }
    

    func createDatePicker() {
        
        //format for picker
        datePicker.datePickerMode = .date
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //bar button item
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: false)
        
        txtDate.inputAccessoryView = toolbar
        
        //assigning date picker to text field
        txtDate.inputView = datePicker
    }
    
    func donePressed() {
        
        //format date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        
        txtDate.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    
    @IBAction func btnDoneTapped(_ sender: AnyObject) {
        
        if txtName?.text != "" && txtMission?.text != "" && txtCountry?.text != "" && txtDate?.text != ""{
            
            let entityDescription = NSEntityDescription.entity(forEntityName: kEntityStr, in: appDelegateObj.managedObjectContext)
            let newPerson = NSManagedObject(entity: entityDescription!, insertInto: appDelegateObj.managedObjectContext)
            
            /*
             let originalText = txtName.text!
             let cipheredText = cipher( originalText, shift:10 )
             */
            
            
  
            let name1 = txtName.text!
            let mission1 = txtMission.text!
            let country1 = txtCountry.text!
            let date1 = txtDate.text!
            
            var tempname = ""
            var tempmission = ""
            var tempcountry = ""
            var tempdate = ""
            
            for ch in name1.characters
            {
                tempname.append(SecurityService.cipher(i: ch))
            }
            
            for ch in mission1.characters
            {
                tempmission.append(SecurityService.cipher(i: ch))
            }
            
            for ch in country1.characters
            {
                tempcountry.append(SecurityService.cipher(i: ch))
            }
            
            for ch in date1.characters
            {
                tempdate.append(SecurityService.cipher(i: ch))
            }
            
            newPerson.setValue(tempname, forKey: kNameStr)
            newPerson.setValue(tempmission, forKey: kMissionStr)
            newPerson.setValue(tempcountry, forKey: kCountryStr)
            newPerson.setValue(tempdate, forKey: kDateStr)
            
            
            do {
                try newPerson.managedObjectContext?.save()
            } catch {
                print("Error occured during save entity")
            }
            

            self.dismiss(animated: true, completion: nil)

            
        }
        else {
            let alert = UIAlertController(title: "Alert", message: "Please fill all form", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        
        }

    }
    
    @IBAction func btnCancelTap(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func cipher1( _ text:String, shift:Int ) -> String {
        
        var textCharArray:[Character] = Array( text.characters )
        
        let alphabet =
            Array("0123456789abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUWVXYZ !".characters);
        
        let offset = alphabet.count // alphabet.length in other languages
        
        for i in 0 ..< text.characters.count {
            
            let oldChar = textCharArray[ i ]
            let oldCharIdx = alphabet.index( of:oldChar ) // get index of
            
            let oldCharIdxUnwrapped = oldCharIdx! // oldCharIdx can be null!
            let newCharIdx = ( oldCharIdxUnwrapped + shift + offset ) % offset
            
            let newChar = alphabet[ newCharIdx ]
            
            textCharArray[ i ] = newChar
        }
        
        return String( textCharArray )
    }
    
    func decipher( _ text:String, shift2:Int ) -> String {
        
        // when calling a function you don't have to specify the first argument...
        // For other arguments you have to specify it!
        return cipher1( text, shift:shift2 * -1 )
    }
    
    
        
    
    

    
}

