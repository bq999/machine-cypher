//
//  map2.swift
//  project2
//
//  Created by Bilal Qaiser on 20/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//


import UIKit
import CoreData
import MapKit
import CoreLocation


class map2: UITableViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate  {
    
    
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var dataArray = [NSManagedObject]()
    
    @IBOutlet weak var mapObj: MKMapView!

    var address : [Address1] = []
    var annotations : [MKPointAnnotation] = []
    var mapManager = CLLocationManager()
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.title = "Map Profile"

        mapManager.delegate = self                            // ViewController is the "owner" of the map.
        mapManager.desiredAccuracy = kCLLocationAccuracyBest  // Define the best location possible to be used in app.
        mapManager.requestWhenInUseAuthorization()            // The feature will not run in background
        mapManager.startUpdatingLocation()                    // Continuously geo-position update
        

        
        loadData()

    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchData()

    }
    
    
    
    
    
    func fetchData() {
        let entityDescription = NSEntityDescription.entity(forEntityName: kEntityStr, in: appDelegateObj.managedObjectContext)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entityDescription
        
        

        
        do {
            dataArray = try appDelegateObj.managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]
            
            
            self.tableView.reloadData()
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! ProfileCell3
       
        let cell2 = dataArray[indexPath.row].value(forKey: kCountryStr) as? String
        
        /*
        var locations = [
            ["title": "USA",    "latitude": 37.090240, "longitude": -95.712891],
            ["title": "Canada", "latitude": 43.653226, "longitude": -79.383184],
            ["title": "Africa",     "latitude": 6.552746, "longitude": 20.390625],
            
            ["title": "Pakistan",    "latitude": 30.375321, "longitude": 69.345116],
            ["title": "India", "latitude": 34.052238, "longitude": 78.962880],
            ["title": "Qatar",     "latitude": 25.354826, "longitude": 51.183884],
            
            ["title": "Carribean",    "latitude": 17.074656, "longitude": -61.817521],
            ["title": "Rome", "latitude": 41.902783, "longitude": 12.496366],
            ["title": "Mexico",     "latitude": 23.634501, "longitude": -102.552784]
        ]
        */
        
        var locations = [
            ["title": "",    "latitude": 0.0, "longitude": 0.0]]
        
        var l2 = [String]()
        var l3 = [String]()
        
        //let cell2 = cell.country.text!
        
        l2.append(cell2!)
        //print(cell2)
        
        if l2.contains("5/2/43/2/7/2/") {
            locations.append(["title": "Canada", "latitude": 43.653226, "longitude": -79.383184])
        }
        
        
        for location in locations {
            
            let annotation = MKPointAnnotation()
            annotation.title = location["title"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: location["latitude"] as! Double, longitude: location["longitude"] as! Double)
            
            
            
            mapObj.addAnnotation(annotation)
            print(annotation)
        }
        
        
        
      
        return cell
        
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            appDelegateObj.managedObjectContext.delete(dataArray[indexPath.row])
            do {
                try appDelegateObj.managedObjectContext.save()
                dataArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } catch {
                let saveError = error as NSError
                print(saveError)
            }
        }
    }
    
    
     func findAddress() {

        /*
        
        let locations = [
            ["title": "New York, NY",    "latitude": 40.713054, "longitude": -74.007228, "color": "red"],
            ["title": "Los Angeles, CA", "latitude": 34.052238, "longitude": -118.243344, "color": "blue"],
            ["title": "Chicago, IL",     "latitude": 41.883229, "longitude": -87.632398, "color": "green"]
        ]
        
        for location in locations {
        
            
            let annotation = MKPointAnnotation()
            annotation.title = location["title"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: location["latitude"] as! Double, longitude: location["longitude"] as! Double)
            
            mapObj.addAnnotation(annotation)
            print(annotation)
        }
        
 
        
        mapObj.removeAnnotations(annotations)
        
        //print(txtaddress.text!)
        
       
        cell.country.text = dataArray[indexPath.row].value(forKey: kCountryStr) as? String
        
        let cell2 = cell.country.text!
        print(cell2)
        */
        
        /*
        let  city = ""
        if let b1 = retrieveData(addressName: city) {
            
            
            
            
            let userAnnotation = MKPointAnnotation()
            userAnnotation.coordinate = CLLocationCoordinate2DMake(b1.address.latitude, b1.address.longitude)
            mapObj.addAnnotation(userAnnotation)
            annotations.append(userAnnotation)
            
            
            // Here we define the map's zoom. The value 0.01 is a pattern
            let zoom:MKCoordinateSpan = MKCoordinateSpanMake(5, 5)
            
            // Store latitude and longitude received from smartphone
            let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(b1.address.latitude, b1.address.longitude)
            
            // Based on myLocation and zoom define the region to be shown on the screen
            let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, zoom)
            
            // Setting the map itself based previous set-up
            mapObj.setRegion(region, animated: true)
            
            
            
        }


        */
    }

    
    
    

    // dismiss the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func loadData(){
        let fairview = Address(name: "Toronto", latitude: 43.7777446, longitude: -79.3469631)
        let airport = Address(name: "Airport", latitude: 43.6777176, longitude: -79.6270084)
        let na = Address(name: "Niagra", latitude: 43.0538471, longitude: -79.2281199)
        let cn = Address(name: "CN", latitude: 43.6425662, longitude: -79.3892455)
        
        let b1 = Address1(address: fairview)
        let b2 = Address1(address: airport)
        let b3 = Address1(address: na)
        let b4 = Address1(address: cn)
        
        address.append(b1)
        address.append(b2)
        address.append(b3)
        address.append(b4)
        
        
    }
    
    func retrieveData(addressName : String) -> Address1? {
        
        for a in address {
            if a.address.addressname == addressName {
                return a
            }
        }
        return nil
    }
    

    
    func retrieveData2(countryName : String) -> Address1? {
        
        let country1 = countryName
        
        
        
        for i  in address {
            
            if (i.address.addressname == country1){
                return i
            }
        }
        return nil
        
    }
    
    @IBAction func map2(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "map2", sender: -1)
    }
    
    
}


class ProfileCell3 : UITableViewCell {
    @IBOutlet weak var country: UILabel!
    
}

