//
//  map.swift
//  project2
//
//  Created by Bilal Qaiser on 23/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//


import UIKit
import CoreData
import MapKit
import CoreLocation




class map: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate  {
    
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
 
    @IBOutlet weak var mapObj: MKMapView!
    @IBOutlet weak var image: UIImageView!
    
    var address : [Address1] = []
    var annotations : [MKPointAnnotation] = []
    var mapManager = CLLocationManager()
    
    let locationManager = CLLocationManager()
    //var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var dataArray = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Map Profile"
        
        mapManager.delegate = self                            // ViewController is the "owner" of the map.
        mapManager.desiredAccuracy = kCLLocationAccuracyBest  // Define the best location possible to be used in app.
       // mapManager.requestWhenInUseAuthorization()            // The feature will not run in background
      //  mapManager.startUpdatingLocation()                    // Continuously geo-position update
        
        activity.stopAnimating()
        activity.hidesWhenStopped = true
        
       // let span:MKCoordinateSpan = MKCoordinateSpanMake(180, 360)
     //   let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(6.552746, 20.390625)
      //  let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
       // mapObj.setRegion(region, animated: true)
        
 
      //  blue.coordinate = myLocation
      //  blue.title = "blue"
     //   blue.imageName = "blue.png"
      //  mapObj.addAnnotation(blue)
        
        findAddress()
        
        /*
         let hello = MyPointAnnotation()
         hello.coordinate = CLLocationCoordinate2D(latitude: 37.090240, longitude: -95.712891)
         hello.pinTintColor = .yellow
         
         let hellox = MyPointAnnotation()
         hellox.coordinate = CLLocationCoordinate2D(latitude: 34, longitude: -72)
         hellox.pinTintColor = .blue
         
         mapObj.addAnnotation(hello)
         mapObj.addAnnotation(hellox)
         */

        
    }


    
    

    
    @IBAction func slideBlue(_ sender: UISlider) {
        
        var slide:[String] = []
        var mission1:[String] = []
        
        for item in dataArray{
            if let country = item.value(forKey: kCountryStr){
                slide.append(country as! String)
            }
        }
        
        
        
        for missionitem in dataArray{
            if let mission = missionitem.value(forKey: kMissionStr){
                mission1.append(mission as! String)
            }
        }
            
        //P - 53/       - Blue
        //I = 23/       - Red
        //R - 61/       - green
        
        
        //USA
        if slide.contains("73/67/2/") {
            updateImage(1)
        }
        
        //Canada
        if slide.contains("5/2/43/2/7/2/") {
            updateImage(2)
            
        }
        
        //Africa
        if slide.contains("2/13/61/23/5/2/") {
            updateImage(3)
            
        }
        

        /*
        //Pakistan
        if (slide.contains("53/2/31/23/67/71/2/43/") && mission1.contains("53/")){
            updateImage(4)
        }
        
        //India
        if (slide.contains("23/43/7/23/2/") && mission1.contains("53/")){
            updateImage(5)
        }
        
        //Qatar
        if (slide.contains("59/2/71/2/61/") && mission1.contains("53/")){
            updateImage(6)
        }


        
        
        //Carribean
        if (slide.contains("5/2/61/61/23/3/11/2/43/") && mission1.contains("53/")){
            updateImage(7)
        }
        
        //Rome
        if (slide.contains("61/47/41/11/") && mission1.contains("53/")){
            updateImage(8)
        }
        
        //Mexico
        if (slide.contains("41/11/89/23/5/47/") && mission1.contains("53/")){
            updateImage(9)
        }
        */
    }
    
    @IBAction func slideGreen(_ sender: UISlider) {
        
        var slide:[String] = []
        var mission1:[String] = []

        for item in dataArray{
            if let country = item.value(forKey: kCountryStr){
                slide.append(country as! String)
            }
        }
        
        /*
        let currentValue = Int(sender.value)
        updateImage(currentValue)
 
        
        //Pakistan
        if slide.contains("53/2/31/23/67/71/2/43/") {
            updateImage(4)
        }
        
        //India
        if slide.contains("23/43/7/23/2/") {
            updateImage(5)
        }
        
        //Qatar
        if slide.contains("59/2/71/2/61/") {
            updateImage(6)
        }
*/
        
        
        for missionitem in dataArray{
            if let mission = missionitem.value(forKey: kMissionStr){
                mission1.append(mission as! String)
            }
        }
        
        //P - 53/       - Blue
        //I = 23/       - Red
        //R - 61/       - green
        
       /*
        //USA
        if (slide.contains("5/2/43/2/7/2/") && mission1.contains("61/")) {
            updateImage(1)
            
        }
        
        //Canada
        if (slide.contains("5/2/43/2/7/2/") && mission1.contains("61/")){
            updateImage(2)
            
        }
        
        //Africa
        if (slide.contains("2/13/61/23/5/2/") && mission1.contains("61/")){
            updateImage(3)
            
        }
        
        */
        
        //Pakistan
        if slide.contains("53/2/31/23/67/71/2/43/") {
            updateImage(4)
        }
        
        //India
        if slide.contains("23/43/7/23/2/") {
            updateImage(5)
        }
        
        //Qatar
        if slide.contains("59/2/71/2/61/") {
            updateImage(6)
        }
        
        
        
        /*
        //Carribean
        if (slide.contains("5/2/61/61/23/3/11/2/43/") && mission1.contains("61/")){
            updateImage(7)
        }
        
        //Rome
        if (slide.contains("61/47/41/11/") && mission1.contains("61/")){
            updateImage(8)
        }
        
        //Mexico
        if (slide.contains("41/11/89/23/5/47/") && mission1.contains("61/")){
            updateImage(9)
        }
        */
    }
    
    @IBAction func slideRed(_ sender: UISlider) {
        
        var slide:[String] = []
        var mission1:[String] = []

        for item in dataArray{
            if let country = item.value(forKey: kCountryStr){
                slide.append(country as! String)
            }
        }
        
/*
        
        //Carribean
        if slide.contains("5/2/61/61/23/3/11/2/43/") {
            updateImage(7)
        }
        
        //Rome
        if slide.contains("61/47/41/11/") {
            updateImage(8)
        }
        
        //Mexico
        if slide.contains("41/11/89/23/5/47/") {
            updateImage(9)
        }*/
        
        
        for missionitem in dataArray{
            if let mission = missionitem.value(forKey: kMissionStr){
                mission1.append(mission as! String)
            }
        }
        
        //P - 53/       - Blue
        //I = 23/       - Red
        //R - 61/       - green
        
        /*
        //USA
        if (slide.contains("5/2/43/2/7/2/") && mission1.contains("23/")) {
            updateImage(1)
            
        }
        
        //Canada
        if (slide.contains("5/2/43/2/7/2/") && mission1.contains("23/")){
            updateImage(2)
            
        }
        
        //Africa
        if (slide.contains("2/13/61/23/5/2/") && mission1.contains("23/")){
            updateImage(3)
            
        }
        
        
        
        //Pakistan
        if (slide.contains("53/2/31/23/67/71/2/43/") && mission1.contains("23/")){
            updateImage(4)
        }
        
        //India
        if (slide.contains("23/43/7/23/2/") && mission1.contains("23/")){
            updateImage(5)
        }
        
        //Qatar
        if (slide.contains("59/2/71/2/61/") && mission1.contains("23/")){
            updateImage(6)
        }
        */
        
        
        
        //Carribean
        if slide.contains("5/2/61/61/23/3/11/2/43/"){
            updateImage(7)
        }
        
        //Rome
        if slide.contains("61/47/41/11/") {
            updateImage(8)
        }
        
        //Mexico
        if slide.contains("41/11/89/23/5/47/") {
            updateImage(9)
        }

        
        
    }
    
    
    
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
       
        let annotationView = MKPinAnnotationView()
        annotationView.pinTintColor = UIColor.green
        
        print("Pin Color Set")
        
        return annotationView
        
        

    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // fetchData()
        
    }
    
    

    
    func findAddress() {
        
       // var country = ["USA","Canada","Pakistan","India","Mexico","Qatar","Carribean","Russia","Brazil"]

        
        var countries:[String] = []
        for item in dataArray{
            if let country = item.value(forKey: kCountryStr){
                countries.append(country as! String)
            }
        }
        /*
         for item in dataArray{
         countries.append(item.value(forKey: kCountryStr) as! String)
         }
         */
       // print(countries)

        
        
        
      //  var locations: [[String: Any]] = [[String: String, "key2": "value2", "key3": "value3"]]

        //var locations = [[String:String],[String:Double],[String:Double]]
        
        var locations = [
            ["title": "",    "latitude": 0.0, "longitude": 0.0]
        ]
        
        //CANADA
        if countries.contains("5/2/43/2/7/2/") {
            locations.append(["title": "Canada", "latitude": 43.653226, "longitude": -79.383184])
        }
        
        //USA
        if countries.contains("73/67/2/") {
            locations.append(["title": "USA",    "latitude": 37.090240, "longitude": -95.712891])
        }
        
        //AFRICA
        if countries.contains("2/13/61/23/5/2/") {
            locations.append(["title": "Africa",     "latitude": 6.552746, "longitude": 20.390625])
        }
        
        //PAKISTAN
        if countries.contains("53/2/31/23/67/71/2/43/") {
            locations.append(["title": "Pakistan",    "latitude": 30.375321, "longitude": 69.345116])
        }
        
        //INDIA
        if countries.contains("23/43/7/23/2/") {
            locations.append(["title": "India", "latitude": 28.704059, "longitude": 77.102490])
        }
        
        //QATAR
        if countries.contains("59/2/71/2/61/") {
            locations.append(["title": "Qatar",     "latitude": 25.354826, "longitude": 51.183884])
        }
        
        //CARRIBEAN
        if countries.contains("5/2/61/61/23/3/11/2/43/") {
            locations.append(["title": "Carribean",    "latitude": 17.074656, "longitude": -61.817521])
        }
        
        //ROME
        if countries.contains("61/47/41/11/") {
            locations.append(["title": "Rome", "latitude": 41.902783, "longitude": 12.496366])
        }
        
        //MEXICO
        if countries.contains("41/11/89/23/5/47/") {
            locations.append(["title": "Mexico",     "latitude": 23.634501, "longitude": -102.552784])
        }
        
        print(locations)
        
        
        
        
        
        
        for location in locations {
            
            let annotation = MKPointAnnotation()
            annotation.title = location["title"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: location["latitude"] as! Double, longitude: location["longitude"] as! Double)

            mapObj.addAnnotation(annotation)
            print(annotation)
        }
        
        
        
        /*
        let locations = [
            ["title": "USA",    "latitude": 37.090240, "longitude": -95.712891],
            ["title": "Canada", "latitude": 43.653226, "longitude": -79.383184],
            ["title": "Africa",     "latitude": 6.552746, "longitude": 20.390625],
            
            ["title": "Pakistan",    "latitude": 30.375321, "longitude": 69.345116],
            ["title": "India", "latitude": 34.052238, "longitude": 78.962880],
            ["title": "Qatar",     "latitude": 25.354826, "longitude": 51.183884],
            
            ["title": "Carribean",    "latitude": 17.074656, "longitude": -61.817521],
            ["title": "Rome", "latitude": 41.902783, "longitude": 12.496366],
            ["title": "Mexico",     "latitude": 23.634501, "longitude": -102.552784]
        ]
        
        for location in locations {
            
            let annotation = MKPointAnnotation()
            annotation.title = location["title"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: location["latitude"] as! Double, longitude: location["longitude"] as! Double)
            

            
            mapObj.addAnnotation(annotation)
            print(annotation)
        }
        */
    }
    
    func updateImage(_ button: Int){
        // starts the download indicator
        activity.startAnimating()
        
        var image:[String] = []
        
        for item in dataArray{
            if let country = item.value(forKey: kCountryStr){
                image.append(country as! String)
            }
        }
        
        
        // getting the image's url
        var url = URL(string: "")
        switch button {
        case 1:
            //USA
            url = URL(string: "https://media-cdn.tripadvisor.com/media/photo-s/0e/9a/e3/1d/freedom-tower.jpg")
        case 2:
            //Canada
            url = URL(string: "https://media.timeout.com/images/102873865/630/472/image.jpg")
        case 3:
            //Africa
            url = URL(string: "http://www.southafrica.co.za/images/south-africa-self-drive-calvinia-fp.jpg")
            
            
        case 4:
            //Pakistan
            url = URL(string: "http://minhajimages.kortechx.netdna-cdn.com/images-sliders/Minhaj-ul-Quran_Islamabad_01.jpg")
        case 5:
            //India
            url = URL(string: "https://cache-graphicslib.viator.com/graphicslib/thumbs360x240/5588/SITours/private-tour-day-trip-to-agra-from-delhi-including-taj-mahal-and-agra-in-delhi-149788.jpg")

        case 6:
            //Qatar
            url = URL(string: "http://www.qatar.northwestern.edu/images/dev-images/examples/1220x500-qatar.jpg")
            
            
        case 7:
            //Carribean
            url = URL(string: "https://c1.staticflickr.com/2/1420/1044619413_09de03d648_b.jpg")
        case 8:
            //Rome
            url = URL(string: "https://cache-graphicslib.viator.com/graphicslib/thumbs674x446/3731/SITours/skip-the-line-ancient-rome-and-colosseum-half-day-walking-tour-in-rome-114992.jpg")
        case 9:
            //Mexico
            url = URL(string: "https://cache-graphicslib.viator.com/graphicslib/thumbs674x446/2895/SITours/mexico-city-sightseeing-tour-in-mexico-city-136502.jpg")
        default:
            break
        }
        
        // creating the background thread
        DispatchQueue.global(qos: DispatchQoS.userInitiated.qosClass).async {
            // Image Download
            let fetch = NSData(contentsOf: url! as URL)
            
            //Creating the main thread, that will update the user interface
            DispatchQueue.main.async {
                
                // Assign image dowloaded to image variable
                if let imageData = fetch {
                    self.image.image = UIImage(data: imageData as Data)
                }
                
                // stops the download indicator
                self.activity.stopAnimating()
            }
        }
    }

    // dismiss the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "map2" {
            let vc : missionVC = segue.destination as! missionVC
            vc.dataArray = dataArray
        }
        
    }
    
    @IBAction func mapMission(_ sender: AnyObject) {
    }


    
    }


