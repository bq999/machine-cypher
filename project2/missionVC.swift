//
//  missionVC.swift
//  project2
//
//  Created by Bilal Qaiser on 25/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//


import UIKit
import CoreData
import MapKit
import CoreLocation
import MobileCoreServices
import MessageUI

class missionVC: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate  {
    
    
    @IBOutlet weak var mapObj: MKMapView!
    
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var dataArray = [NSManagedObject]()
    
    
    var address : [Address1] = []
    var annotations : [MKPointAnnotation] = []
    var mapManager = CLLocationManager()
    
    @IBOutlet weak var photo: UIImageView!
    
    
    @IBOutlet weak var redText: UILabel!
    @IBOutlet weak var greenText: UILabel!
    @IBOutlet weak var blueText: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Map Mission"
        
        
        
        mapManager.delegate = self                            // ViewController is the "owner" of the map.
        mapManager.desiredAccuracy = kCLLocationAccuracyBest  // Define the best location possible to be used in app.
        mapManager.requestWhenInUseAuthorization()            // The feature will not run in background
        mapManager.startUpdatingLocation()                    // Continuously geo-position update
        self.mapObj.showsUserLocation = true
        
        
        findAddress()
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let mylocation = CLLocation(latitude: 43.773257, longitude: -79.335899)

        let NY = CLLocation(latitude: 40.712784, longitude: -74.005941)
        let india = CLLocation(latitude: 28.704059, longitude: 77.102490)
        let carribean = CLLocation(latitude: 17.074656, longitude: -61.817521)


        let distance = NY.distance(from: mylocation) / 1000
        let y = Double(round(10*distance)/10)

        greenText.text = "\(y) KM"
        
        let distance1 = india.distance(from: mylocation) / 1000
        let y1 = Double(round(10*distance1)/10)

        blueText.text = "\(y1) KM"
        
        let distance2 = carribean.distance(from: mylocation) / 1000
        let y2 = Double(round(10*distance2)/10)

        redText.text = "\(y2) KM"
    }
    
    
    
    
    
    @IBAction func camera(_ sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func camera2(_ sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        
    }
    

    
    
    
    @IBAction func camera3(_ sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    
    
    
    
    // MARK: Photo Controller
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        self.dismiss(animated: true, completion: nil)
        print("-------> imagePickerController antes do if")
        if mediaType.isEqual(to: kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            print("associando imagem")
            photo.image = image
            UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
        }
    }
    


    @IBAction func mail(_ sender: Any) {
        
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }

    }
    
    
    @IBAction func mail2(_ sender: Any) {
        
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }
        
    }
    
    
    
    
    @IBAction func mail3(_ sender: Any) {
        
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }
        
    }
    
    
    func configureMailController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients(["noname@hotmail"])
        mailComposerVC.setSubject("Hello")
        mailComposerVC.setMessageBody("How are you doing?", isHTML: false)
        
        return mailComposerVC
    }
    
    
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    
    

    
    
    func findAddress() {

        var countries:[String] = []
        for item in dataArray{
            if let country = item.value(forKey: kCountryStr){
                countries.append(country as! String)
            }
        }

        print(countries)
        
 
        var locations = [
            ["title": "",    "latitude": 0.0, "longitude": 0.0]
        ]
        
        //CANADA
        if countries.contains("5/2/43/2/7/2/") {
            locations.append(["title": "Canada", "latitude": 43.653226, "longitude": -79.383184])
        }
        
        //USA
        if countries.contains("73/67/2/") {
            locations.append(["title": "USA",    "latitude": 37.090240, "longitude": -95.712891])
        }
        
        //AFRICA
        if countries.contains("2/13/61/23/5/2/") {
            locations.append(["title": "Africa",     "latitude": 6.552746, "longitude": 20.390625])
        }
        
        //PAKISTAN
        if countries.contains("53/2/31/23/67/71/2/43/") {
            locations.append(["title": "Pakistan",    "latitude": 30.375321, "longitude": 69.345116])
        }
        
        //INDIA
        if countries.contains("23/43/7/23/2/") {
            locations.append(["title": "India", "latitude": 28.704059, "longitude": 77.102490])
        }
        
        //QATAR
        if countries.contains("59/2/71/2/61/") {
            locations.append(["title": "Qatar",     "latitude": 25.354826, "longitude": 51.183884])
        }
        
        //CARRIBEAN
        if countries.contains("5/2/61/61/23/3/11/2/43/") {
            locations.append(["title": "Carribean",    "latitude": 17.074656, "longitude": -61.817521])
        }
        
        //ROME
        if countries.contains("61/47/41/11/") {
            locations.append(["title": "Rome", "latitude": 41.902783, "longitude": 12.496366])
        }
        
        //MEXICO
        if countries.contains("41/11/89/23/5/47/") {
            locations.append(["title": "Mexico",     "latitude": 23.634501, "longitude": -102.552784])
        }
        
        print(locations)
        
  
        for location in locations {
            
            let annotation = MKPointAnnotation()
            annotation.title = location["title"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: location["latitude"] as! Double, longitude: location["longitude"] as! Double)
            
            mapObj.addAnnotation(annotation)
            print(annotation)
        }
        

    }
    
    
    
    
    // dismiss the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
}


