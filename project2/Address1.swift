//
//  Address1.swift
//  project2
//
//  Created by Bilal Qaiser on 20/07/2017.
//  Copyright © 2017 Bilal Qaiser. All rights reserved.
//

import Foundation

class Address1 {
    
    var address : Address
    
    init(name: String, latitude: Double, longitude: Double)
    {
        self.address = Address(name: name, latitude: latitude, longitude: longitude)
        
    }
    
    init(address: Address){
        self.address = address
    }
    
    
    
}
